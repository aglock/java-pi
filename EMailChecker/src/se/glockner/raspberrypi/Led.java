/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package se.glockner.raspberrypi;

import com.oracle.deviceaccess.PeripheralManager;
import com.oracle.deviceaccess.gpio.GPIOPin;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author andreas
 */
public class Led {
    private final int pinNo;
    private final String name;
    private GPIOPin pin;
    
    public Led(int pinNo, String name) throws IOException{
        this.pinNo = pinNo;
        this.name = name;
        pin = PeripheralManager.open(pinNo);
    }
    
    public void on(){
        try {
            pin.setValue(true);
            System.out.println(name);
        } catch (IOException ex) {
            Logger.getLogger(Led.class.getName()).log(Level.SEVERE, null, ex);
        }
    } 
    
    public void off(){
        try {
            pin.setValue(false);
        } catch (IOException ex) {
            Logger.getLogger(Led.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void destroy(){
        try {
            pin.close();
        } catch (IOException ex) {
            Logger.getLogger(Led.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
