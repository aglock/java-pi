/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package se.glockner.raspberrypi;

import com.oracle.deviceaccess.PeripheralConfig;
import com.oracle.deviceaccess.PeripheralManager;
import com.oracle.deviceaccess.gpio.GPIOPin;
import com.oracle.deviceaccess.gpio.GPIOPinConfig;
import com.oracle.deviceaccess.gpio.PinListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author andreas
 */
public class Button {
    private final int pinNo;
    private final String name;
    private GPIOPin pin;
    
    public Button(int pinNo, String name) throws IOException{
        this.pinNo = pinNo;
        this.name = name;
        GPIOPinConfig gpioPinConfig = new GPIOPinConfig(0, pinNo, GPIOPinConfig.DIR_INPUT_ONLY, 
                PeripheralConfig.DEFAULT, GPIOPinConfig.TRIGGER_RISING_EDGE, false);
        pin = PeripheralManager.open(gpioPinConfig);
        
    }
    
    public void setInputListener(PinListener pinListener) throws IOException{
        pin.setInputListener(pinListener);
    }
    
    public void destroy(){
        try {
            pin.close();
        } catch (IOException ex) {
            Logger.getLogger(Button.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String toString(){
        return name;
    }
    
}
