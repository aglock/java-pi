/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package se.glockner.email;

import com.oracle.deviceaccess.gpio.PinEvent;
import com.oracle.deviceaccess.gpio.PinListener;
import javax.microedition.midlet.MIDlet;
import se.glockner.raspberrypi.Button;
import se.glockner.raspberrypi.Led;


/**
 *
 * @author andreas
 */
public class EmailChecker extends MIDlet {
    
    private Button checkButton;
    private Led newMailLed;
    private Led checkMailLed;
    
    public void startApp() {
        try{
            checkButton = new Button(17, "CheckMail Button");
            newMailLed = new Led(18, "New Mail Arrived");
            checkMailLed = new Led(23, "Checking Mail");
            
            checkButton.setInputListener(new PinListener(){
                public void valueChanged(PinEvent event){
                    System.out.println(checkButton);
                    checkMailLed.on();
                    newMailLed.off();
                    try {
                        Thread.sleep(1000);
                    }catch(InterruptedException e){
                        // NOOp
                    }
                    checkMailLed.off();
                    newMailLed.on();
                }
                
            });
        }catch(Exception e){
            System.out.println("Exception: " + e);
            notifyDestroyed();
        }
    }
    public void pauseApp() {
    }
    
    public void destroyApp(boolean unconditional) {
        if (newMailLed != null) {
            newMailLed.off();
            newMailLed.destroy();
        }
        if (checkMailLed != null) {
            checkMailLed.off();
            checkMailLed.destroy();
        }
        if (checkButton != null) {
            checkButton.destroy();
        }
        
    }
}

