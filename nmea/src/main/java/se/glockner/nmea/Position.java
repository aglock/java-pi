package se.glockner.nmea;

public interface Position {
	
	/**
	 * 
	 * @return a String on the format [N|S]ddmm.mmmm
	 */
	public String getLatitude();
	
	/**
	 * 
	 * @return a String on the format [E|W]￼dddmm.mmmm
	 */
	public String getLongitude();

}
