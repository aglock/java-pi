package se.glockner.nmea;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class ParseUtils {
	
	private static final String delim = ",";

	public static DataType extractDataType(String sentence) {
		String[] words = split(sentence);
		String dataTypeWord = words[0];
		String dataType = dataTypeWord.substring(3,6);
		return DataType.getValueFrom(dataType);
	}
	
	public static String parseCheckSum(String NMEAString){
		int lastWord = NMEAString.lastIndexOf(",");
		String checkSumStr = NMEAString.substring(lastWord+1);
		if(!checkSumStr.startsWith("*")){
			return null;
		}
      final String checkSumString = checkSumStr.substring(1, checkSumStr.length()-2);
      System.out.println("checksum: ["+checkSumString + "]");
		return checkSumString;
	}
        
        public static String[] split(String sentence){
            return split(sentence, delim);
        }
        
        
        public static String[] split(String sentence, String delimiter){
      	  //System.out.println("Splitting: " + sentence);
            StringTokenizer st = new StringTokenizer(sentence, delimiter, true);
            List<String> tokens = new ArrayList<String>();
            while(st.hasMoreTokens()){
            	String s = st.nextToken();
            	if(s.equals(","))
            		tokens.add("");
            	else 
            		tokens.add(s);
            }
           // System.out.println(tokens);
            for(int i = 0; i < tokens.size(); i++){
            	String token = tokens.get(i);
            	//System.out.println(i+ " " + token);
            	if(token.equals("")){
            		String nextToken = tokens.get(i+1);
            		if(!nextToken.equals("")){
            			tokens.remove(i);
            		}
            	}
            }
            //System.out.println(tokens);
            //System.out.println("Token size = " + tokens.size());
            return tokens.toArray(new String[tokens.size()]);
        }
        
        public static Float stringToFloat(String s){
      	  if(s.equals("")){
      		  return null;
      	  } else {
      		  return Float.valueOf(s);
      	  }
        }
        
        public static Integer stringToInteger(String s){
      	  if(s.equals("")){
      		  return null;
      	  } else {
      		  return Integer.valueOf(s);
      	  }
        }

	

}
