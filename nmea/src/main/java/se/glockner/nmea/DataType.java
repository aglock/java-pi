package se.glockner.nmea;

public enum DataType {
	GGA,
	GSA,
	GSV,
	RMC,
	VTG,
	UNKNOWN;
	
	public static DataType getValueFrom(String s){
		try{
			return DataType.valueOf(s);
		}catch(IllegalArgumentException e){
			return DataType.UNKNOWN;
		}
	}
	
}
