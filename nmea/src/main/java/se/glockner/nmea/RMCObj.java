package se.glockner.nmea;

public class RMCObj extends NMEAData implements Position{

	public RMCObj(String sentence) {
		super(sentence);
	}

	public String getLatitude() {
		return words[4] + words[3];
	}

	public String getLongitude() {
		return words[6] + words[5];
	}
	
	public String getTime(){
		return words[1];
	}
	
	public String getStatus(){
		return words[2];
	}

	public Float getSpeed() {			
		if(words[7] == null){
			return null;
		}
		return Float.parseFloat(words[7]);
	}

	public Float getCourse() {
		if(words[8] == null){
			return null;
		}
		return Float.parseFloat(words[8]);
	}

	public Float getVariation() {
		if(words[10] == null){
			return null;
		}
		return Float.parseFloat(words[10]);
	}

	public String getDate() {
		return words[9];
	}

	public String getVariationType() {
		return words[11];
	}
	
	public ModeType getMode(){
		return ModeType.valueOf(words[12]);
	}
	
	
	

}
