package se.glockner.nmea;

public enum FixIndicator {
	FIX_NOT_AVAILABLE(0, "Fix not available"),
	GPS_FIX(1, "GPS fix"),
	DGPS_FIX(2, "DGPS fix");
	
	int id;
	String text;
	
	private FixIndicator(int id, String text){
		this.id = id;
		this.text = text;
	}
	
	public static FixIndicator valueOf(Integer i){
		if(i==null){
			return FIX_NOT_AVAILABLE;
		}
		switch(i){
			case 0:
				return FIX_NOT_AVAILABLE;
			case 1:
				return GPS_FIX;
			case 2:
				return DGPS_FIX;
			default:
				return FIX_NOT_AVAILABLE;
		}
	}

}
