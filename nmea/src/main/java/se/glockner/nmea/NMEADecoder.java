package se.glockner.nmea;

public class NMEADecoder {

	private boolean sentenceValid = false;
	private String sentence;

	public boolean isSentenceValid() {
		return sentenceValid;
	}

	public NMEAData decode(String sentence) {
		if(sentence == null){
			return null; //Nothing to decode
		}
		this.sentence = sentence;
		sentenceValid = validateSentence();
		if(sentenceValid){
			DataType dt = ParseUtils.extractDataType(sentence);
			switch(dt){
				case GGA:
					return new GGAObj(sentence);
				default:
					return null;
			}
		}
		return null;
	}

	private boolean validateSentence() {
		/*
		 * Each sentence begins with a '$' and ends with a carriage return/line, can be no longer than 80 characters 
		 * of visible text (plus the line terminators). The data items separated by 
		 * commas. The data ascii text and may extend over multiple sentences in certain specialized instances but is 
		 * normally fully contained in one variable length sentence. For example time might be indicated to decimal parts of 
		 * a second or location may be show with 3 or even 4 digits 
		 * after the decimal point. Programs that read the data should not depend on column positions. 
		 * There is a provision for a checksum at the end of each sentence which may or may not be checked 
		 * by the unit that reads the data. The checksum field consists of a '*' and two hex digits representing an 
		 * 8 bit exclusive OR of all characters between, but not including, the '$' and '*'. 
		 * A checksum is required on some sentences.
		 */
		boolean valid = false;
		if(!sentence.startsWith("$") || sentence.length() > 81 || !sentence.endsWith("\n")){
			return valid;
		}
		valid = true;
		return valid;
	}
	
}
