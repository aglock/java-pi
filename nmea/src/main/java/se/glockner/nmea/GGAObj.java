package se.glockner.nmea;

public class GGAObj extends NMEAData implements Position{
	
	public GGAObj(String ggaSentence){
		super(ggaSentence);
	}

	public String getTime() {
		return words[1];
	}

	public String getLatitude() {
		return words[3] + words[2];
	}

	public String getLongitude() {
		return words[5] + words[4];
	}

	public PositionFix getPositionFix() {
		FixIndicator fi = FixIndicator.valueOf(Integer.parseInt(words[6]));
		Integer satUsed = ParseUtils.stringToInteger(words[7]);
		Float hdop = ParseUtils.stringToFloat(words[8]);
		Float alt = ParseUtils.stringToFloat(words[9]);
		String altUnit = words[10];
		Float gs = ParseUtils.stringToFloat(words[11]);
		String gsUnit = words[12];
		return PositionFix.Builder.newBuilder()
						.fixIndicator(fi)
						.satelitesUsed(satUsed)
						.HDOP(hdop)
						.altitude(alt, altUnit)
						.gedodialSeparation(gs, gsUnit)
						.build();
	}
		
}
