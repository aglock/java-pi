package se.glockner.nmea;

public abstract class NMEAData {
	
	protected String sentence;
	protected DataType dataType;
	protected String checksum;
	protected String[] words;
	
	public NMEAData(String sentence){
		this.sentence = sentence;
		parse();
	}
	
	public DataType getDataType(){
		return dataType;
	}
	
	public String getCheckSum(){
		return checksum;
	}
	
	public String getRawMessage(){
		return sentence;
	}
	
	private void parse() {
		words = ParseUtils.split(sentence);
		dataType = ParseUtils.extractDataType(sentence);
		checksum = ParseUtils.parseCheckSum(sentence);
		
	}

}
