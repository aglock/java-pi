package se.glockner.nmea;

public class PositionFix {

	private Integer satelitesUsed;
	private FixIndicator fixIndicator;
	private Float HDOP;
	private Float altitude;
	private String altUnit;
	private Float geodialSep;
	private String geodialSepUnit;
	
	private PositionFix(){
		
	}
	
	public int getSatelitesUsed() {
		return satelitesUsed;
	}

	public FixIndicator getFixIndicator() {
		return fixIndicator;
	}

	public Float getHDOP() {
		return HDOP;
	}

	public Float getAltitude() {
		return altitude;
	}

	public String getAltUnit() {
		return altUnit;
	}

	public Float getGeodialSep() {
		return geodialSep;
	}

	public String getGeodialSepUnit() {
		return geodialSepUnit;
	}

	public static class Builder {
		private PositionFix instance;
		
		public static Builder newBuilder(){
			return new Builder();
		}
		
		public Builder(){
			instance = new PositionFix();
		}
		
		public Builder satelitesUsed(int n){
			instance.satelitesUsed = n;
			return this;
		}
		public Builder fixIndicator(FixIndicator fi){
			instance.fixIndicator = fi;
			return this;
		}
		
		public Builder HDOP(Float hdop){
			instance.HDOP = hdop;
			return this;
		}
		
		public Builder altitude(Float alt, String unit){
			instance.altitude = alt;
			instance.altUnit = unit;
			return this;
		}
		public Builder gedodialSeparation(Float geodialSep, String unit){
			instance.geodialSep = geodialSep;
			instance.geodialSepUnit = unit;
			return this;
		}
		
		public PositionFix build(){
			return instance;
		}
	}
	
}
