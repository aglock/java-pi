package se.glockner.nmea;

import org.junit.Assert;
import org.junit.Test;

public class TestParseUtils {
	
	private static final String nmeaWithCheckSum = "$GPGGA,064951.000,2307.1256,N,12016.4438,E,1,8,0.95,39.9,,*65\r\n";
	private static final String nmeaWithoutCheckSum = "$GPGGA,064951.000,2307.1256,N,12016.4438,E,1,8,0.95,39.9,M,17.8,M\n";
	
	@Test
	public void testCheckSumParse(){
		String checkSum = ParseUtils.parseCheckSum(nmeaWithCheckSum);
		Assert.assertEquals("65", checkSum);
		
	}
	
	@Test
	public void testCheckSumParseWithOutCheckSum(){
		String checkSum = ParseUtils.parseCheckSum(nmeaWithoutCheckSum);
		Assert.assertNull(checkSum);
	}
	
	@Test
	public void testSplitComplete(){
		String[] words = ParseUtils.split(nmeaWithCheckSum);
		Assert.assertEquals(12, words.length);
		for(int i = 0; i < words.length; i++){
			if(i!=13) // 13 is optional
			Assert.assertNotNull(i + " is null", words[i]);
			
		}
	}
	
	@Test
	public void splitTest(){
		final String s = "$GPGGA,180133.084,,,,,0,0,,,M,,M,,*4C";
		String[] words = ParseUtils.split(s);
		Assert.assertEquals(15, words.length);
		for(int i = 0; i < 15; i++){
			Assert.assertFalse(i + " starts with ',': " + words[i], words[i].startsWith(","));
		}
	}

}
