package se.glockner.nmea;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestGGAObj {
	
	
	private GGAObj gga;
        private String incompleteGGA = "$GPGGA,180133.084,,,,,0,0,,,M,,M,,*4C";
        private String GGA = "$GPGGA,064951.000,2307.1256,N,12016.4438,E,1,8,0.95,39.9,M,17.8,M,,*65\r\n";

	/**
	 * $GPGGA,064951.000,2307.1256,N,12016.4438,E,1,8,0.95,39.9,M,17.8,M,,*65
	 * 
	 * Message ID 		$GPGGA ￼ ￼ ￼ GGA protocol header
		UTC Time ￼ 		064951.000 hhmmss.sss
		Latitude ￼ ￼ 		2307.1256 ￼ ￼ ddmm.mmmm
		N/S Indicator N N=north or S=south
		Longitude ￼ ￼ 	12016.4438 ￼ ￼ dddmm.mmmm
		E/W Indicator E E=east or W=west
		Position Fix 1 See Table-3 Indicator
		Satellites Used 8 Range 0 to 14
		HDOP ￼ ￼ 			0.95 ￼ ￼ Horizontal Dilution of Precision
		MSL Altitude 	39.9 meters Antenna Altitude above/below mean-sea-level
		Units ￼ ￼ M ￼ meters Units of antenna altitude
		Geoidal Separation 17.8 meters
		Units ￼ ￼ M ￼ meters Units of geoids separation
		Age of Diff. Corr. second Null fields when DGPS is not used
	 */
	@Before
	public void setup(){
		gga = new GGAObj(GGA);
	}
	
	@Test
	public void testParseToNMEAData(){
		Assert.assertTrue(gga.getDataType() == DataType.GGA);
		Assert.assertEquals(GGA, gga.getRawMessage());
	}
	
	@Test
	public void testParseTime(){
		Assert.assertEquals("064951.000", gga.getTime());
	}
	
	@Test
	public void testParseLatitude(){
		Assert.assertEquals("N2307.1256", gga.getLatitude());
		
	}
	
	@Test
	public void testParseLongitude(){
		Assert.assertEquals("E12016.4438", gga.getLongitude());
	}
	
	@Test
	public void testChecksum(){
		Assert.assertEquals("65", gga.getCheckSum());
	}
	
	@Test
	public void testParseFixData(){
		PositionFix pf = gga.getPositionFix();
		Assert.assertEquals(8, pf.getSatelitesUsed());
		Assert.assertEquals(FixIndicator.GPS_FIX, pf.getFixIndicator());
		Assert.assertEquals(0.95, pf.getHDOP(), 0.1);
		Assert.assertEquals(39.9, pf.getAltitude(), 0.1);
		Assert.assertEquals("M", pf.getAltUnit());
		Assert.assertEquals(17.8, pf.getGeodialSep(), 0.1);
		Assert.assertEquals("M", pf.getGeodialSepUnit());
	}
	
	@Test
	public void testIncompleteData(){
		gga = new GGAObj(incompleteGGA);
		PositionFix pf = gga.getPositionFix();
		Assert.assertEquals(0, pf.getSatelitesUsed());
		Assert.assertEquals(FixIndicator.FIX_NOT_AVAILABLE, pf.getFixIndicator());
		Assert.assertNull(pf.getHDOP());
		Assert.assertNull(pf.getAltitude());
		Assert.assertEquals("M", pf.getAltUnit());
		Assert.assertNull(pf.getGeodialSep());
		Assert.assertEquals("M", pf.getGeodialSepUnit());
	}
	
	

}
