package se.glockner.nmea;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestSentenceIsNMEA {
	
	static final String NMEAString = "$GPAAM,A,A,0.10,N,WPTNME*32\n";
	static final String nonNMEAString="R$GPAAM,A,A,0.10,N,WPTNME*32A";

	private NMEADecoder decoder;

	@Before
	public void setup(){
		decoder = new NMEADecoder();
		
	}
	
	@Test
	public void testInvalidNMEASentence(){
		decoder.decode(nonNMEAString);
		Assert.assertFalse(decoder.isSentenceValid());
	}
	
	@Test
	public void testValidNMEASentence(){
		decoder.decode(NMEAString);
		Assert.assertTrue(decoder.isSentenceValid());
	}
	
}


