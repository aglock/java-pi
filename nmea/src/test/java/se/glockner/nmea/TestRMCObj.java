package se.glockner.nmea;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestRMCObj {
	
	private static final String RMC_SENTENCE = "$GPRMC,064951.000,A,2307.1256,N,12016.4438,E,0.03,165.48,260406,3.05,W,A,*2C\r\n";
	private RMCObj rmc;
	
	@Before
	public void setup(){
		rmc = new RMCObj(RMC_SENTENCE);
	}
	
	@Test
	public void testTime(){
		Assert.assertEquals("064951.000", rmc.getTime());
	}
	
	@Test
	public void testStatus(){
		Assert.assertEquals("A", rmc.getStatus());
	}
	
	@Test
	public void testLatitude(){
		Assert.assertEquals("N2307.1256", rmc.getLatitude());
	}
	
	@Test 
	public void testLongitude(){
		Assert.assertEquals("E12016.4438", rmc.getLongitude());
	}
	
	@Test
	public void testSpeed(){
		Assert.assertEquals(0.03, rmc.getSpeed(), 0.01);
	}
	
	@Test
	public void testCourse(){
		Assert.assertEquals(165.48, rmc.getCourse(), 0.01);
	}
	
	@Test
	public void testMagneticVariation(){
		Assert.assertEquals(3.05, rmc.getVariation(), 0.01);
		Assert.assertEquals("W", rmc.getVariationType());
	}
	
	@Test
	public void testModeType(){
		Assert.assertEquals(ModeType.A, rmc.getMode());
	}
	
	
	@Test
	public void testDate(){
		Assert.assertEquals("260406", rmc.getDate());
	}
	
	@Test
	public void testChecksum(){
		Assert.assertEquals("2C", rmc.getCheckSum());
	}

}
